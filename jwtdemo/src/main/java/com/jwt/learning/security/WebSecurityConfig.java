package com.jwt.learning.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	 @Override
	 protected void configure(HttpSecurity http) throws Exception {
		 
		 http
		    .authorizeRequests()
		    .antMatchers("/**").permitAll()
		    .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		
		 http.headers().xssProtection();
		 
	 }

}
