package com.jwt.learning.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import com.jwt.learning.ConfigClass.DatabaseConfig;

public class RoleDB implements IRole {
		
	public RoleDB(boolean ejecutar) {
		if(ejecutar) {
			crearTablaRoles();
			funcionEncontrarRol();
			funcionEncontrarRolNombre();
			funcionCrearRol();
			funcionEditarRol();
			funcionEliminarRol();
		}
	}
	
	@Override
	public void crearTablaRoles() {
		
		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	         String sql = "CREATE TABLE IF NOT EXISTS Role " +
	            "(ID INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY, " +
	            "Name VARCHAR(128) NOT NULL UNIQUE)";
	         
	         stmt.executeUpdate(sql);
	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	      } 
	
	}

	@Override
	public void funcionEncontrarRol() {
		// TODO Auto-generated method stub

		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	         stmt.execute("CREATE OR REPLACE FUNCTION encontrarRol(rolID INT)\r\n" + 
	         		"RETURNS SETOF role AS $$\r\n" + 
	        		 
	         		"BEGIN\r\n" + 
	         		"  RETURN QUERY SELECT id, name FROM role WHERE id = rolID;\r\n" + 
	         		"END\r\n" + 
	         		
	         		"$$ LANGUAGE plpgsql;");

	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	      }
	}
	
	@Override
	public void funcionEncontrarRolNombre() {
		// TODO Auto-generated method stub

		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	         stmt.execute("CREATE OR REPLACE FUNCTION existeRol(rolNombre VARCHAR)\r\n" + 
	         		"RETURNS SETOF role AS $$\r\n" + 
	        		 
	         		"BEGIN\r\n" + 
	         		"  RETURN QUERY SELECT id, name FROM role WHERE UPPER(name) = rolNombre;\r\n" + 
	         		"END\r\n" + 
	         		
	         		"$$ LANGUAGE plpgsql;");

	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	      }
	}

	@Override
	public void funcionCrearRol() {
		// TODO Auto-generated method stub

		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	         stmt.execute("CREATE OR REPLACE FUNCTION crearRol(nombreRol VARCHAR)\r\n" + 
	        		"RETURNS SETOF role AS $$\r\n" + 
	        		 
	        		"BEGIN\r\n" + 
	         		"INSERT INTO role (name) VALUES (nombreRol);\r\n" + 
	         		"RETURN QUERY SELECT id, name FROM role ORDER BY id DESC LIMIT 1;\r\n" +
	         		"END\r\n" + 
	         		
	         		"$$ LANGUAGE plpgsql;");

	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	      }
	}

	@Override
	public void funcionEditarRol() {
		// TODO Auto-generated method stub

		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	         stmt.execute("CREATE OR REPLACE FUNCTION editarRol(rolID INT, nuevoNombre VARCHAR)\r\n" + 
	        		"RETURNS SETOF role AS $$\r\n" + 
	        		 
		        	"BEGIN\r\n" + 
	         		"UPDATE role SET name = nuevoNombre WHERE id = rolID;\r\n" + 
	         		"RETURN QUERY SELECT id, name FROM role WHERE id = rolID;\r\n" + 
	         		"END\r\n" + 
	         		
	         		"$$ LANGUAGE plpgsql;");

	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	      }
	}

	@Override
	public void funcionEliminarRol() {
		// TODO Auto-generated method stub

		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	         stmt.execute("CREATE OR REPLACE FUNCTION eliminarRol(rolID INT)\r\n" + 
	        		"RETURNS SETOF role AS $$\r\n" + 
	        		 
			        "BEGIN\r\n" + 
			        "RETURN QUERY SELECT id, name FROM role WHERE id = rolID;\r\n" + 
	         		"DELETE FROM role WHERE id = rolID;\r\n" + 
	         		"END\r\n" + 
	         		
	         		"$$ LANGUAGE plpgsql;");

	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName() + ": " + e.getMessage());
	      }
	}
	
}
