package com.jwt.learning;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConfigClass {
	
	private static ConfigClass config;
	
	@Autowired
	public void setConfigClass(ConfigClass objeto){
		ConfigClass.config = objeto;
	}
	
	@Value("${spring.datasource.driverClassName}")
    private String driver;

    @Value("${spring.datasource.url}")
    private String URL;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;
    
    // Getters
    
    public String getDriver() {
    	return this.driver;
    }
    
    public String getURL() {
    	return this.URL;
    }
    
    public String getUsername() {
    	return this.username;
    }
    
    public String getPassword() {
    	return this.password;
    }
    
    public static class DatabaseConfig {
    	public static String dbURL = config.getURL();
    	public static String dbDriver = config.getDriver();
    	public static String dbUsername = config.getUsername();
    	public static String dbPassword = config.getPassword();
    }

}
