package com.jwt.learning.controllers;

import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.learning.models.Account;
import com.jwt.learning.security.BCrypt.Encriptacion;
import com.jwt.learning.security.jwt.JWT;
import com.jwt.learning.security.jwt.JWTFilter;
import com.jwt.learning.services.UserService.UserServices;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/usuarios")
public class UserController {
		
	// Encontrar un usuario por medio de su ID.
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<LinkedHashMap<String, Object>> getRol(@PathVariable int id, 
			HttpServletRequest request, HttpServletResponse response) throws SQLException {
		
		boolean accesoAutorizado = JWTFilter.loginJWT(request);
		
		// El token es valido, acceder al servicio.
		if(accesoAutorizado) {
			response.addHeader("Fecha", new Date().toString());
			response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");
		    response.setHeader("Pragma","no-cache");
		    response.setDateHeader("Expires", 0);
			
			Account usuarioEncontrado = UserServices.buscarUsuario(id);
			
			// No se encontro un usuario con esta ID, retornar nulo.
			if(usuarioEncontrado == null) {
				response.setStatus(404);
				return null;
			}
			// Existe un usuario con esta ID, proceder.
			else 
			{
				LinkedHashMap<String, Object> jsonResponse = new LinkedHashMap<String, Object>();
				jsonResponse.put("id", usuarioEncontrado.getID());
				jsonResponse.put("name", usuarioEncontrado.getName());
				jsonResponse.put("roleFK", usuarioEncontrado.getRoleFK());
				return new ResponseEntity<LinkedHashMap<String, Object>>(jsonResponse, HttpStatus.OK);
			}
		} 
		// No ha sido autorizado, denegar acceso.
		else 
		{
			response.setStatus(401);
			return null;
		}
    }
	
	// Agregar un nuevo usuario
	
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<LinkedHashMap<String, Object>> postUsuario(@Valid @RequestBody Account usuario, 
			HttpServletRequest request, HttpServletResponse response) throws SQLException {
		
		LinkedHashMap<String, Object> jsonResponse = new LinkedHashMap<String, Object>();
		
		if(UserServices.buscarUsuario(usuario.getName()) == null) {
			// Validar que la contraseña posea una letra mayuscula, una minuscula y un numero
			if(!usuario.getPassword().matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{12,}$")) {
				jsonResponse.put("Error", 400);
				jsonResponse.put("Mensaje", "La contraseña debe poseer al menos 12 caracteres, "
						+ "una letra mayuscula, una letra minuscula y un numero");
				
				return new ResponseEntity<LinkedHashMap<String, Object>>(jsonResponse, HttpStatus.BAD_REQUEST);
			}
			
			Account usuarioCreado = UserServices.crearUsuario(usuario);
			jsonResponse.put("id", usuarioCreado.getID());
			jsonResponse.put("name", usuarioCreado.getName());
			jsonResponse.put("roleFK", usuarioCreado.getRoleFK());
			
			return new ResponseEntity<LinkedHashMap<String, Object>>(jsonResponse, HttpStatus.CREATED);
		}
		else
		{
			jsonResponse.put("Error", 409);
			jsonResponse.put("Mensaje", "Ya se encuentra registrado un usuario con este nombre");
			
			return new ResponseEntity<LinkedHashMap<String, Object>>(jsonResponse, HttpStatus.CONFLICT);
		}
    }
	
	// Editar un usuario existente
	
	@PutMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<LinkedHashMap<String, Object>> updateUsuario(@PathVariable int id, @RequestBody Account usuario,
			HttpServletRequest request, HttpServletResponse response) throws SQLException {
		
		boolean accesoAutorizado = JWTFilter.loginJWT(request);
		
		// El token es valido, acceder al servicio.
		if(accesoAutorizado) {
			Account usuarioActualizado = UserServices.editarUsuario(id, usuario);
			LinkedHashMap<String, Object> jsonResponse = new LinkedHashMap<String, Object>();
			
			// No se encontro ningun usuario con este ID.
			if(usuarioActualizado == null) {
				response.setStatus(404);
				return null;
			}
			// En caso contrario, retornar la respuesta en JSON.
			else 
			{
				response.addHeader("URL", request.getServerName() + ":" +  request.getServerPort() + "/usuarios/" + usuarioActualizado.getID());
				response.addHeader("Fecha", new Date().toString());
				response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");
			    response.setHeader("Pragma","no-cache");
			    response.setDateHeader("Expires", 0);
			    
				jsonResponse.put("id", usuarioActualizado.getID());
				jsonResponse.put("name", usuarioActualizado.getName());
				jsonResponse.put("roleFK", usuarioActualizado.getRoleFK());
				
				return new ResponseEntity<LinkedHashMap<String, Object>>(jsonResponse, HttpStatus.OK);
			}
		} 
		// No ha sido autorizado, denegar acceso.
		else 
		{
			response.setStatus(401);
			return null;
		}
    }
	
	// Eliminar un usuario existente
	
	@DeleteMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<LinkedHashMap<String, Object>> deleteUsuario(@PathVariable int id,
			HttpServletRequest request, HttpServletResponse response) throws SQLException {
		
		boolean accesoAutorizado = JWTFilter.loginJWT(request);
		
		// El token es valido, acceder al servicio.
		if(accesoAutorizado) {
			Account usuarioEliminado = UserServices.eliminarUsuario(id);
			
			// No existe un usuario con este ID.
			if(usuarioEliminado == null) {
				response.setStatus(404);
				return null;
			}
			// Si existe, eliminarlo y retornar la informacion en JSON.
			else 
			{
				response.addHeader("Fecha", new Date().toString());
				response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");
			    response.setHeader("Pragma","no-cache");
			    response.setDateHeader("Expires", 0);
			    
			    LinkedHashMap<String, Object> jsonResponse = new LinkedHashMap<String, Object>();
				jsonResponse.put("id", usuarioEliminado.getID());
				jsonResponse.put("name", usuarioEliminado.getName());
				jsonResponse.put("roleFK", usuarioEliminado.getRoleFK());
				
				return new ResponseEntity<LinkedHashMap<String, Object>>(jsonResponse, HttpStatus.OK);
			}
		} 
		// No ha sido autorizado, denegar acceso.
		else 
		{
			response.setStatus(401);
			return null;
		}
    }
	
	// Ingresar al sistema por medio de credenciales de usuario.

	@PostMapping(value = "/login", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> login(@RequestBody Account usuario, 
			HttpServletResponse response) throws SQLException {
		
		// Identificar usuario con sus credenciales
		Account usuarioIdentificado = UserServices.login(usuario);
		// Si las credenciales son correctas, retornar un token.
		if(usuarioIdentificado != null) {
			JWT token = new JWT();
			// Crear token con los credenciales de usuario.
			String JWTcreado = token.crearJWT(usuarioIdentificado);
			// Agregar token al Header de autorizacion.
			response.addHeader("Authorization", "Bearer " + JWTcreado);
			return new ResponseEntity<String>(JWTcreado, HttpStatus.OK);
		}
		// Credenciales invalidos, retornar error.
		else {
			return new ResponseEntity<String>("Credenciales invalidos!", HttpStatus.UNAUTHORIZED);
		}
    }
	
	// Cambiar la contraseña de usuario

	@PostMapping(value = "/cambiarpass")
	public ResponseEntity<String> cambiarContraseña(HttpServletRequest request, HttpServletResponse response,
			@RequestBody Map<String, String> credenciales) throws SQLException {
	
		// Obtener contraseña nueva y vieja
		String viejaPass = credenciales.get("viejaPass");
		String nuevaPass = credenciales.get("nuevaPass");
		// Verificar que el usuario haya accedido al sistema.
		boolean accesoAutorizado = JWTFilter.loginJWT(request);
		
		// El token es valido, acceder al servicio.
		if(accesoAutorizado) {
			// Validaciones para las contraseñas
			if(viejaPass.length() < 12 || nuevaPass.length() < 12) {
				return new ResponseEntity<String>("Las contraseñas debe contener al menos 12 caracteres", HttpStatus.BAD_REQUEST);
			}
			if(viejaPass.equals(nuevaPass)) {
				return new ResponseEntity<String>("La contraseña nueva y vieja no pueden ser las mismas", HttpStatus.BAD_REQUEST);
			}
			// Validar que la contraseña posea una letra mayuscula, una minuscula y un numero
			if(!nuevaPass.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{12,}$")) {		
				return new ResponseEntity<String>("La contraseña debe poseer al menos 12 caracteres,"
						+ " una letra mayuscula, una letra minuscula y un numero", HttpStatus.BAD_REQUEST);
			}
			// Extraer token del header por medio de expresiones regulares.
			String headerToken = request.getHeader("Authorization");
			String[] valoresToken = headerToken.split(" ");
			String token = valoresToken[1];
			// Desencriptar cuerpo del token.
			String tokenDesencriptado = JWT.desencriptarCuerpo(token);
			// Conseguir sujeto desde los credenciales del token.
			String usuario = JWT.getSujeto(tokenDesencriptado);
			// Conseguir contraseña hasheada del usuario a partir del nombre en su token.
			String contraseñaHasheada = UserServices.conseguirContraseña(usuario);
			// Las antiguas contraseñas coinciden, actualizar contraseña.
			if(Encriptacion.verificarContraseña(viejaPass, contraseñaHasheada)) {
				Account nuevosDatos = new Account();
				nuevosDatos.setName(usuario);
				nuevosDatos.setPassword(viejaPass);
				UserServices.cambiarContraseña(nuevosDatos, nuevaPass);
				response.setHeader("Authorization", null);
				return new ResponseEntity<String>("Contraseña actualizada", HttpStatus.OK);
			}
			// Error en la comparacion de contraseñas, retornar error.
			else 
			{
				return new ResponseEntity<String>("Error de contraseñas", HttpStatus.BAD_REQUEST);
			}
			
		}
		// Usuario no autorizado, no confiar en este token.
		else 
		{
			// Retornar error, codigo HTTP 401 ( No Autorizado )
			response.setStatus(401);
			return null;
		}
	}
    

}
