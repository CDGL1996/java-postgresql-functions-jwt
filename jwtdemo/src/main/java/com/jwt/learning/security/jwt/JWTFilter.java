package com.jwt.learning.security.jwt;

import javax.servlet.http.HttpServletRequest;

public class JWTFilter {

	public static boolean loginJWT(HttpServletRequest request) {
		JWT jwt = new JWT();
		String token = null;
		
		if(request.getHeader("Authorization") != null) {
			// Extraer token del header.
			String headerToken = request.getHeader("Authorization");
			String[] valoresToken = headerToken.split(" ");
			token = valoresToken[1];

			// Desencriptar el token y validarlo.
			String tokenDesencriptado = JWT.desencriptarCuerpo(token);
			// Ocurrio un fallo al desencriptar el token, rechazar acceso.
			if(tokenDesencriptado == null) {
				return false;
			}
			return jwt.validarToken(tokenDesencriptado);
		}
		// No hay header Authorization, denegar acceso.
		else 
		{
			return false;
		}
	}
	
}
