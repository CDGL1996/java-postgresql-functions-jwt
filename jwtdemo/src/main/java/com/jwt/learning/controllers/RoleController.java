package com.jwt.learning.controllers;

import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.learning.models.Role;
import com.jwt.learning.security.jwt.JWT;
import com.jwt.learning.security.jwt.JWTFilter;
import com.jwt.learning.services.RoleService.RoleServices;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/roles")
public class RoleController {
	
	// Encontrar un rol por medio de su ID.
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<Role> getRol(@PathVariable int id,
			HttpServletRequest request, HttpServletResponse response) throws SQLException {

		boolean accesoAutorizado = JWTFilter.loginJWT(request);
		
		// El token es valido, acceder al servicio.
		if(accesoAutorizado) {
			
			// Verificar si el usuario posee los privilegios correctos.
            String headerToken = request.getHeader("Authorization");
            String[] valoresToken = headerToken.split(" ");
            String token = valoresToken[1];
            // Desencriptar el token y validarlo.
         	String tokenDesencriptado = JWT.desencriptarCuerpo(token);
            // Si el usuario es administrador o desarrollador, acceder al servicio.
            if(JWT.getRol(tokenDesencriptado).equalsIgnoreCase("Administrador") ||
                    JWT.getRol(tokenDesencriptado).equalsIgnoreCase("Dev")) {

            	Role rolEncontrado = RoleServices.buscarRol(id);
				
    			if(rolEncontrado == null) {
    				response.setStatus(404);
    				return null;
    			}
    			else 
    			{
    				response.addHeader("Fecha", new Date().toString());
    				response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");
    				response.setHeader("Pragma","no-cache");
    				response.setDateHeader("Expires", 0);
    					
    				return new ResponseEntity<Role>(rolEncontrado, HttpStatus.OK);
    			}
            }
            // El usuario no es administrador ni desarrollador, retornar error de autorizacion.
            else {
                response.setStatus(401);
                return null;
            }
		}
		// No ha sido autorizado, denegar acceso.
		else 
		{
			response.setStatus(401);
			return null;
		}
    }
	
	// Crear un nuevo rol.
	
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<LinkedHashMap<String, Object>> postRol(@Valid @RequestBody Role rol, 
			HttpServletRequest request, HttpServletResponse response) throws SQLException {
		
		boolean accesoAutorizado = JWTFilter.loginJWT(request);
		
		// El token es valido, acceder al servicio.
		if(accesoAutorizado) {
			
			// Verificar si el usuario posee los privilegios correctos.
            String headerToken = request.getHeader("Authorization");
            String[] valoresToken = headerToken.split(" ");
            String token = valoresToken[1];
            // Desencriptar el token y validarlo.
         	String tokenDesencriptado = JWT.desencriptarCuerpo(token);
            // Si el usuario es administrador o desarrollador, acceder al servicio.
            if(JWT.getRol(tokenDesencriptado).equalsIgnoreCase("Administrador") ||
                    JWT.getRol(tokenDesencriptado).equalsIgnoreCase("Dev")) {

            	LinkedHashMap<String, Object> jsonResponse = new LinkedHashMap<String, Object>();
    			
    			if(RoleServices.buscarRol(rol.getName()) == null) {
    				Role rolCreado = RoleServices.crearRol(rol);
    				
    				response.addHeader("URL", request.getServerName() + ":" +  request.getServerPort() + "/roles/" + rolCreado.getID());
    				response.addHeader("Fecha", new Date().toString());
    				response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");
    			    response.setHeader("Pragma","no-cache");
    			    response.setDateHeader("Expires", 0);
    			    				
    				jsonResponse.put("id", rolCreado.getID());
    				jsonResponse.put("name", rolCreado.getName());
    				
    				return new ResponseEntity<LinkedHashMap<String, Object>>(jsonResponse, HttpStatus.CREATED);
    			}
    			else
    			{
    				jsonResponse.put("Error", 409);
    				jsonResponse.put("Mensaje", "Ya se encuentra registrado un rol con este nombre");
    				
    				return new ResponseEntity<LinkedHashMap<String, Object>>(jsonResponse, HttpStatus.CONFLICT);
    			}
            }
            // El usuario no es administrador ni desarrollador, retornar error de autorizacion.
            else {
                response.setStatus(401);
                return null;
            }
		} 
		// No ha sido autorizado, denegar acceso.
		else 
		{
			response.setStatus(401);
			return null;
		}
    }
	
	// Actualizar un rol existente.
	
	@PutMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<LinkedHashMap<String, Object>> updateRol(@Valid @PathVariable int id, @Valid @RequestBody Role rol,
			HttpServletRequest request, HttpServletResponse response) throws SQLException {
		
		boolean accesoAutorizado = JWTFilter.loginJWT(request);
		
		// El token es valido, acceder al servicio.
		if(accesoAutorizado) {
			
			// Verificar si el usuario posee los privilegios correctos.
            String headerToken = request.getHeader("Authorization");
            String[] valoresToken = headerToken.split(" ");
            String token = valoresToken[1];
            // Desencriptar el token y validarlo.
         	String tokenDesencriptado = JWT.desencriptarCuerpo(token);
            // Si el usuario es administrador o desarrollador, acceder al servicio.
            if(JWT.getRol(tokenDesencriptado).equalsIgnoreCase("Administrador") ||
                    JWT.getRol(tokenDesencriptado).equalsIgnoreCase("Dev")) {

            	LinkedHashMap<String, Object> jsonResponse = new LinkedHashMap<String, Object>();
    			Role rolEncontrarID = RoleServices.buscarRol(id);
    			Role rolEncontrarNombre = RoleServices.buscarRol(rol.getName());
    			
    			// No existe un rol con esta ID.
    			if(rolEncontrarID == null) {
    				response.setStatus(404);
    				return null;
    			}
    			// En caso de encontrar un rol
    			if(rolEncontrarNombre != null) {
    				// Verificar si ya existe un rol con ese nombre en la base de datos
    				if(rolEncontrarNombre.getID() != id && rol.getName().equalsIgnoreCase(rolEncontrarNombre.getName())) {
    					response.setStatus(409);
    					jsonResponse.put("Error", 409);
    					jsonResponse.put("Mensaje", "Ya se encuentra registrado un rol con este nombre");
    					return new ResponseEntity<LinkedHashMap<String, Object>>(jsonResponse, HttpStatus.CONFLICT);
    				}
    			}
    			// Si todas las validaciones han sido aprobadas, ejecutar el servicio.
    			else 
    			{
    				Role rolActualizado = RoleServices.editarRol(id, rol);
    				response.addHeader("URL", request.getServerName() + ":" +  request.getServerPort() + "/roles/" + rolActualizado.getID());
    				response.addHeader("Fecha", new Date().toString());
    				response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");
    			    response.setHeader("Pragma","no-cache");
    			    response.setDateHeader("Expires", 0);
    			    
    				jsonResponse.put("id", rolActualizado.getID());
    				jsonResponse.put("name", rolActualizado.getName());
    			}
    			return new ResponseEntity<LinkedHashMap<String, Object>>(jsonResponse, HttpStatus.OK);
            }
            // El usuario no es administrador ni desarrollador, retornar error de autorizacion.
            else {
                response.setStatus(401);
                return null;
            }
		} 
		// No ha sido autorizado, denegar acceso.
		else 
		{
			response.setStatus(401);
			return null;
		}
    }
	
	// Eliminar un rol existente.
	
	@DeleteMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<LinkedHashMap<String, Object>> deleteRol(@PathVariable int id,
			HttpServletRequest request, HttpServletResponse response) throws SQLException {
		
		boolean accesoAutorizado = JWTFilter.loginJWT(request);
		
		// El token es valido, acceder al servicio.
		if(accesoAutorizado) {
			
			// Verificar si el usuario posee los privilegios correctos.
            String headerToken = request.getHeader("Authorization");
            String[] valoresToken = headerToken.split(" ");
            String token = valoresToken[1];
            // Desencriptar el token y validarlo.
         	String tokenDesencriptado = JWT.desencriptarCuerpo(token);
            // Si el usuario es administrador o desarrollador, acceder al servicio.
            if(JWT.getRol(tokenDesencriptado).equalsIgnoreCase("Administrador") ||
                    JWT.getRol(tokenDesencriptado).equalsIgnoreCase("Dev")) {

            	Role rolEliminado = RoleServices.eliminarRol(id);
    			
    			// No existe un rol con este ID.
    			if(rolEliminado == null) {
    				response.setStatus(404);
    				return null;
    			}
    			// Si existe, eliminarlo y retornar la informacion en JSON.
    			else 
    			{
    				response.addHeader("Fecha", new Date().toString());
    				response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");
    			    response.setHeader("Pragma","no-cache");
    			    response.setDateHeader("Expires", 0);
    			    
    			    LinkedHashMap<String, Object> jsonResponse = new LinkedHashMap<String, Object>();
    				jsonResponse.put("id", rolEliminado.getID());
    				jsonResponse.put("name", rolEliminado.getName());
    				
    				return new ResponseEntity<LinkedHashMap<String, Object>>(jsonResponse, HttpStatus.OK);
    			}
            }
            // El usuario no es administrador ni desarrollador, retornar error de autorizacion.
            else {
                response.setStatus(401);
                return null;
            }
		} 
		// No ha sido autorizado, denegar acceso.
		else 
		{
			response.setStatus(401);
			return null;
		}
    }
	

}
