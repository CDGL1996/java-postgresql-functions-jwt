package com.jwt.learning.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Service;
import com.jwt.learning.ConfigClass.DatabaseConfig;
import com.jwt.learning.models.Account;
import com.jwt.learning.security.BCrypt.Encriptacion;

@Service
public class UserService {
		
	public static class UserServices {
		
		// Retorna un usuario por medio de su ID
		public static Account buscarUsuario(int id) throws SQLException {
			
			// Inicializacion de variables.
			Connection c = null;
			Account usuarioEncontrado = new Account();
			String datosUsuario = null;
			
			try {
				 // Conexion a la base de datos.
				c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
		         // Desactivar el auto commit para realizar transacciones y asegurar la integridad de la BD.
		         c.setAutoCommit(false);

		         // Ejecutar la funcion en PostgreSQL con sus respectivos parametros (ID del usuario a encontrar)
		         PreparedStatement pstmt = c.prepareStatement("SELECT encontrarUsuario(?)");
		         pstmt.setInt(1, id);
		         pstmt.execute();
		         
		         // Obtener los valores retornados por la funcion ejecutada
		         ResultSet results = (ResultSet) pstmt.getResultSet();
		         while (results.next()) {
		        	 datosUsuario = results.getString(1);
		         }
		         // No existe usuario con este ID, retornar nulo.
		         if(datosUsuario == null) {
		        	 return null;
		         }
		         // Cerrar la conexion y el PreparedStatement.   
		         c.commit();
		         pstmt.close();
		         
		         String[] valores = datosUsuario.split(",(?! )");
		         
		         // Si no se encuentra ningun usuario, retornar nulo.
		         if(valores[1].length() < 2) {
		        	 return null;
		         }
		         else 
		         {
		        	 // Obtener los valores necessarios por medio de expresiones regulares.
		        	 int usuarioID = Integer.parseInt(valores[0].replaceAll("\\D+",""));
			         String nombreUsuario = valores[1].replaceAll("[^A-Za-z ]+","");
			         int rolFK = Integer.parseInt(valores[2].replaceAll("\\D+",""));
			         
			         // Rellenar el objeto con los datos retornados de la funcion.
			         usuarioEncontrado.setID(usuarioID);
			         usuarioEncontrado.setName(nombreUsuario);
			         usuarioEncontrado.setRoleFK(rolFK);
		         }
		         
		      } catch ( Exception e ) {
		    	  // En caso de error, retornar los cambios hechos en la BD con un rollback.
		         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		         c.rollback();
		      }
			finally 
			{
				// Por medida preventiva, agregar un fin de conexion que siempre se ejecute.
				if(c != null) c.close();
			}
	        return usuarioEncontrado;
		}
		
		// Retorna un usuario por medio de su nombre
		public static Account buscarUsuario(String nombre) throws SQLException {
					
			// Inicializacion de variables.
			Connection c = null;
			Account usuarioEncontrado = new Account();
			String datosUsuario = null;
					
			try {
				// Conexion a la base de datos.
				c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
				// Desactivar el auto commit para realizar transacciones y asegurar la integridad de la BD.
				c.setAutoCommit(false);

				// Ejecutar la funcion en PostgreSQL con sus respectivos parametros (Nombre del usuario a encontrar)
				PreparedStatement pstmt = c.prepareStatement("SELECT existeUsuario(?)");
				pstmt.setString(1, nombre.toUpperCase().trim());
				pstmt.execute();
				         
				// Obtener los valores retornados por la funcion ejecutada
				ResultSet results = (ResultSet) pstmt.getResultSet();
				while (results.next()) {
					datosUsuario = results.getString(1);
				}
				// No existe usuario con este nombre, retornar nulo.
				if(datosUsuario == null) {
					return null;
				}
				// Cerrar la conexion y el PreparedStatement.   
				c.commit();
				pstmt.close();
				         
				String[] valores = datosUsuario.split(",(?! )");
				         
				// Si no se encuentra ningun usuario, retornar nulo.
				if(valores[1].length() < 2) {
					return null;
				}
				else 
				{
				// Obtener los valores necessarios por medio de expresiones regulares.
				int usuarioID = Integer.parseInt(valores[0].replaceAll("\\D+",""));
				String nombreUsuario = valores[1].replaceAll("[^A-Za-z ]+","");
				int rolFK = Integer.parseInt(valores[2].replaceAll("\\D+",""));
					         
				// Rellenar el objeto con los datos retornados de la funcion.
				usuarioEncontrado.setID(usuarioID);
				usuarioEncontrado.setName(nombreUsuario);
				usuarioEncontrado.setRoleFK(rolFK);
				}         
			} catch ( Exception e ) {
				// En caso de error, retornar los cambios hechos en la BD con un rollback.
				System.err.println( e.getClass().getName()+": "+ e.getMessage() );
				c.rollback();
			}
			finally 
			{
			// Por medida preventiva, agregar un fin de conexion que siempre se ejecute.
				if(c != null) c.close();
			}
			return usuarioEncontrado;
		}

		// Crea un nuevo usuario
		public static Account crearUsuario(Account usuario) throws SQLException {
			
			// Inicializacion de variables.
			Connection c = null;
			Account usuarioCreado = new Account();
			String datosUsuario = null;
			
			try {
				 // Conexion a la base de datos.
				c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
				 // Desactivar el auto commit para realizar transacciones y asegurar la integridad de la BD.
		         c.setAutoCommit(false);

		         // Ejecutar la funcion en PostgreSQL con sus respectivos parametros (Nombre, Contraseña encriptada, ID del Rol)
		         PreparedStatement pstmt = c.prepareStatement("SELECT crearUsuario(?, ?, ?)");
		         pstmt.setString(1, usuario.getName().trim());
		         pstmt.setString(2, Encriptacion.encriptarContraseña(usuario.getPassword()));
		         pstmt.setInt(3, usuario.getRoleFK());
		         pstmt.execute();
		         
		         // Obtener los valores retornados por la funcion ejecutada
		         ResultSet results = (ResultSet) pstmt.getResultSet();
		         while (results.next()) {
		             datosUsuario = results.getString(1);
		         }
		         // Cerrar la conexion y el PreparedStatement.   
		         c.commit();
		         pstmt.close();
		         
		         String[] valores = datosUsuario.split(",(?! )");
		         
		         // Obtener los valores necessarios por medio de expresiones regulares.
		         int usuarioID = Integer.parseInt(valores[0].replaceAll("\\D+",""));
		         String nombreUsuario = valores[1].replaceAll("[^A-Za-z ]+","");
		         int rolFK = Integer.parseInt(valores[2].replaceAll("\\D+",""));
		         
		         // Rellenar el objeto con los datos retornados de la funcion.
		         usuarioCreado.setID(usuarioID);
		         usuarioCreado.setName(nombreUsuario);
		         usuarioCreado.setRoleFK(rolFK);
		         
		      } catch ( Exception e ) {
		    	 // En caso de error, retornar los cambios hechos en la BD con un rollback.
		         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		         c.rollback();
		      }
			finally 
			{
				// Por medida preventiva, agregar un fin de conexion que siempre se ejecute.
				if(c != null) c.close();
			}
	        return usuarioCreado;
		}
		
		// Edita un usuario existente
		public static Account editarUsuario(int id, Account usuario) throws SQLException {
			
			// Inicializacion de variables.
			Connection c = null;
			Account usuarioActualizado = new Account();
			String datosUsuario = null;
			
			try {
				// No existe un usuario con ese ID.
				 if(buscarUsuario(id) == null) {
					 return null;
				 }
				 // Conexion a la base de datos.
				 c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
				 // Desactivar el auto commit para realizar transacciones y asegurar la integridad de la BD.
		         c.setAutoCommit(false);

		         // Ejecutar la funcion en PostgreSQL con sus respectivos parametros (ID del usuario a editar, nuevo nombre y nuevo rol)
		         PreparedStatement pstmt = c.prepareStatement("SELECT editarUsuario(?, ?, ?)");
		         pstmt.setInt(1, id);
		         pstmt.setString(2, usuario.getName());
		         pstmt.setInt(3, usuario.getRoleFK());
		         pstmt.execute();
		         
		         // Obtener los valores retornados por la funcion ejecutada
		         ResultSet results = (ResultSet) pstmt.getResultSet();
		         while (results.next()) {
		             datosUsuario = results.getString(1);
		         }
		         // No existe usuario con este ID, retornar nulo.
		         if(datosUsuario == null) {
		        	 return null;
		         }   
		         // Cerrar la conexion y el PreparedStatement.  
		         c.commit();
		         pstmt.close();
		         
		         String[] valores = datosUsuario.split(",(?! )");
		         
		         // Obtener los valores necessarios por medio de expresiones regulares.
		         int usuarioID = Integer.parseInt(valores[0].replaceAll("\\D+",""));
		         String nombreUsuario = valores[1].replaceAll("[^A-Za-z ]+","");
		         int rolFK = Integer.parseInt(valores[2].replaceAll("\\D+",""));
		         
		         // Rellenar el objeto con los datos retornados de la funcion.
		         usuarioActualizado.setID(usuarioID);
		         usuarioActualizado.setName(nombreUsuario);
		         usuarioActualizado.setRoleFK(rolFK);
		         
		      } catch ( Exception e ) {
		    	 // En caso de error, retornar los cambios hechos en la BD con un rollback.
		         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		         c.rollback();
		      }
			finally 
			{
				// Por medida preventiva, agregar un fin de conexion que siempre se ejecute.
		        if(c != null) c.close();
			}
	        return usuarioActualizado;
		}
		
		// Elimina un usuario existente
		public static Account eliminarUsuario(int id) throws SQLException {
			
			// Inicializacion de variables.
			Connection c = null;
			Account usuarioEliminado = new Account();
			String datosUsuario = null;
			
			try {
				// No existe un usuario con ese ID.
				 if(buscarUsuario(id) == null) {
					 return null;
				 }
				 // Conexion a la base de datos.
				 c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
				 // Desactivar el auto commit para realizar transacciones y asegurar la integridad de la BD.
		         c.setAutoCommit(false);

		         // Ejecutar la funcion en PostgreSQL con sus respectivos parametros (ID del usuario a eliminar)
		         PreparedStatement pstmt = c.prepareStatement("SELECT eliminarUsuario(?)");
		         pstmt.setInt(1, id);
		         pstmt.execute();
		         
		         // Obtener los valores retornados por la funcion ejecutada
		         ResultSet results = (ResultSet) pstmt.getResultSet();
		         while (results.next()) {
		             datosUsuario = results.getString(1);
		         }
		         // No existe usuario con este ID, retornar nulo.
		         if(datosUsuario == null) {
		        	 return null;
		         }
		         // Cerrar la conexion y el PreparedStatement.  
		         c.commit();
		         pstmt.close();
		         
		         String[] valores = datosUsuario.split(",(?! )");
		         
		         // Obtener los valores necessarios por medio de expresiones regulares.
		         int usuarioID = Integer.parseInt(valores[0].replaceAll("\\D+",""));
		         String nombreUsuario = valores[1].replaceAll("[^A-Za-z ]+","");
		         int rolFK = Integer.parseInt(valores[2].replaceAll("\\D+",""));
		         
		         // Rellenar el objeto con los datos retornados de la funcion.
		         usuarioEliminado.setID(usuarioID);
		         usuarioEliminado.setName(nombreUsuario);
		         usuarioEliminado.setRoleFK(rolFK);
		         
		      } catch ( Exception e ) {
		    	 // En caso de error, retornar los cambios hechos en la BD con un rollback.
		         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		         c.rollback();
		      }
			finally 
			{
				// Por medida preventiva, agregar un fin de conexion que siempre se ejecute.
		        if(c != null) c.close();
			}
	        return usuarioEliminado;
		}
		
		// Ingresar al sistema por medio de usuario y contraseña
		public static Account login(Account credencialesUsuario) throws SQLException {
			
			// Inicializacion de variables.
			Connection c = null;
			Account usuarioAutorizado = new Account();
			String datosUsuario = null;
			
			try {
				 // Conexion a la base de datos.
				c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
				 // Desactivar el auto commit para realizar transacciones y asegurar la integridad de la BD.
		         c.setAutoCommit(false);

		         // Ejecutar la funcion en PostgreSQL con sus respectivos parametros (nombre del usuario)
		         PreparedStatement pstmt = c.prepareStatement("SELECT login(?)");
		         pstmt.setString(1, credencialesUsuario.getName().toUpperCase().trim());
		         pstmt.execute();
		         ResultSet results = (ResultSet) pstmt.getResultSet();

		         while (results.next()) {
		             datosUsuario = results.getString(1);
		         }
		         // No se ha encontrado un usuario con este nombre.
		         if(datosUsuario.equals("(,,)") == true) {
		        	 return null;
		         }
		         
		         // Cerrar la conexion y el PreparedStatement.  
		         c.commit();
		         pstmt.close();
		         
		         // Fragmentando los datos en 3 cadenas distintas.
		         String[] valores = datosUsuario.split(",(?! )");
		         
		         // Obteniendo nombre de usuario por medio de expresiones regulares.
		         String nombreUsuario = valores[0].replaceAll("[^A-Za-z ]+","");
		         // Obteniendo contraseña hasheada.
		         String contraseñaUsuario = valores[1];
		         // Obteniendo ID del rol del usuario por medio de expresiones regulares.
		         int rolFK = Integer.parseInt(valores[2].replaceAll("\\D+",""));
		         
		         // Si la contraseña es la correcta, retornar el objeto con la informacion del usuario.
		         if(Encriptacion.verificarContraseña(credencialesUsuario.getPassword(), contraseñaUsuario) == true) { 
		        	 usuarioAutorizado.setName(nombreUsuario);
		        	 usuarioAutorizado.setRoleFK(rolFK);
		 			 return usuarioAutorizado;
		         }
		         // En caso contrario, retornar nulo si los credenciales no son validos.
		         else {
		        	 return null;
		         }
		         
		      } catch ( Exception e ) {
		    	// En caso de error, retornar los cambios hechos en la BD con un rollback.
		         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		         c.rollback();
		      }
			finally 
			{
				// Por medida preventiva, agregar un fin de conexion que siempre se ejecute.
				if(c != null) c.close();
			}
			return usuarioAutorizado;
		}
		
		// Conseguir contraseña encriptada para validarla al momento de cambiar contraseña.
		public static String conseguirContraseña(String usuario) throws SQLException {
			
			// Inicializacion de variables.
			Connection c = null;
			String contraseña = null;
			String datosUsuario = null;

				try {
					 // Conexion a la base de datos.
					c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
					 // Desactivar el auto commit para realizar transacciones y asegurar la integridad de la BD.
			         c.setAutoCommit(false);

			         // Ejecutar la funcion en PostgreSQL con sus respectivos parametros (nombre del usuario)
			         PreparedStatement pstmt = c.prepareStatement("SELECT conseguirPass(?)");
			         pstmt.setString(1, usuario.toUpperCase().trim());
			         pstmt.execute();
			         ResultSet results = (ResultSet) pstmt.getResultSet();
			         
			         while (results.next()) {
			             datosUsuario = results.getString(1);
			         }
			         // Extraer contraseña con expresion regular
			         String contraseñaExtraida = datosUsuario.replace("(", "").replace(")", "");
			         contraseña = contraseñaExtraida;
			         
			      } catch ( Exception e ) {
			    	// En caso de error, retornar los cambios hechos en la BD con un rollback.
			         System.err.println( e.getClass().getName() + " : " + e.getMessage() );
			         c.rollback();
			      }
				finally 
				{
					// Por medida preventiva, agregar un fin de conexion que siempre se ejecute.
					if(c != null) c.close();
				}
				return contraseña;
		}
		
		// Ingresar al sistema por medio de usuario y contraseña
		public static void cambiarContraseña(Account credencialesUsuario, String nuevaContraseña) throws SQLException {
					
			// Inicializacion de variables.
			Connection c = null;
			
			// Los credenciales son correctos, actualizar contraseña.
			if(login(credencialesUsuario) != null) {
				try {
					 // Conexion a la base de datos.
					c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
					 // Desactivar el auto commit para realizar transacciones y asegurar la integridad de la BD.
			         c.setAutoCommit(false);

			         // Ejecutar la funcion en PostgreSQL con sus respectivos parametros (nombre del usuario)
			         PreparedStatement pstmt = c.prepareStatement("SELECT cambiarPass(?, ?)");
			         pstmt.setString(1, credencialesUsuario.getName().toUpperCase().trim());
			         pstmt.setString(2, Encriptacion.encriptarContraseña(nuevaContraseña));
			         pstmt.execute();
			         
			         pstmt.close();
			         c.commit();
			         
			      } catch ( Exception e ) {
			    	// En caso de error, retornar los cambios hechos en la BD con un rollback.
			         System.err.println( e.getClass().getName() + " : " + e.getMessage() );
			         c.rollback();
			      }
				finally 
				{
					// Por medida preventiva, agregar un fin de conexion que siempre se ejecute.
					if(c != null) c.close();
				}
			}
		}
		
		
	}

}
