package com.jwt.learning.db;

public interface IAccount {
	
	void crearTablaUsuario();
	
	void funcionEncontrarUsuario();
	
	void funcionEncontrarUsuarioNombre();
	
	void funcionCrearUsuario();
	
	void funcionEditarUsuario();
	
	void funcionEliminarUsuario();
	
	void funcionLogin();
	
	void funcionConseguirContraseña();

	void funcionCambiarContraseña();

}
