package com.jwt.learning.security.jwt;

import java.security.Key;
import java.security.spec.KeySpec;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.util.StringUtils;

import com.jwt.learning.models.Account;
import com.jwt.learning.models.Role;
import com.jwt.learning.services.RoleService.RoleServices;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWT {
			
	private final String[] arrSecretos = 
		{
				"Xn2r5u8x/A?D(G+KbPeSgVkYp3s6v9y$B&E)H@McQfTjWmZq4t7w!z%C*F-JaNdR",
				"&5QL+U/QEek+(gEW!d)1DnO(wDESRKtaqkPfVMQFQCLlb6CPwXfcheAMmN?V5zr%",
				"&tUQcNiYRBcNyRnZKH7-H4MaB+7gAEfYa?Ksz8E3GbWZI(-u+q@%(yUwKAMN%V@s",
				"(D8Yhv+PV(19XSH0ZA%2FUEcRM?J1p3trqp9x8Zqv1K7$QH-xkFJcFjHyy@1VMf&",
				"qP@LUq@Jcbw-STnnr?Z!9/%T)r4azlS3+F/Y6&U4YJ)jPMEXgK5rW)tlhYLdAO+3",
				"ym6Ek@xH$4)w4eGbWbE&TiIw8D!atgX0lAQt0X9rb)/LS0mh$wcS-4KId3YOC+%!",
				"(&R/hV&(bQ47XjfM%2K?)h/)$(Fs-V8Ki(qVnF4g!WLlF7bwqBNjLQUUEQsqfS%n",
				"93/YJH%Pzb@bzuh&2@J6pqBpTyNOP$aVE2Y773ZDV3%AMuLmprgcGS-a3PRX((RK",
				"dKnKIv8)23h$XuUBlL7zE&@QaHGOomOk&aqVit&2paZXr2q-ruOR)Ddpv/x4(Ih(",
				"+peiFA&+Xhot8eMA@5Ntb9E?iZ?lDuZ%vj3xoourFt-zsivyaozJWj/+GYi7H@Rf",
				"Z81t)YiMqFuBVc!HZJh512KhCdF/l+nYU&NG6/m33Lm6Cd1O$+&BpxWnnrozb73X",
				"QZ9yEx!)M1an1LqK%OhxynO8TlE59+3plq4BmvqjQMAV/NJ8HVmM3-?k&t6Fgx(B",
				"kzZylGa/nGgUPj7y4%w?kYZidseXIjIAC%sq@Jkl+AA3ESwb1Y!ee&GlSzTYGhzN",
				"Axv%rXbX?lDpXj7)IV3PGaWoIiO6qnz4StwKJ&p1!5&i&r!&FU%+ZY7i0Yhp?ACF",
				"kP7binr45&)7?dI?cCQdd&%/ey1gUf)5zFwTO(UyZ$FO-rm4AJUtCQ2lZ/Q+BUOd",
				"(HEQmsGnWMMfUpHQ+@CF06(+e9@6&jK6iKsy6aXELR!z3EL&wQHcvULfN2(Ojxkk",
				"PJ$8&cQmblXXJtSom-+TW)CGz7T(Qo6D)+E%tjl4G)/j)QMn(JpWPPy%(c6Wr7Fu",
				"XpGuPPqV@!Ob-+B7QDrKnxVQm7XJC0vIMIf3U+uuJ-2&xJRqJcM+fw&Tx5Y837P!",
				"y1C(XXWBwnKpDksOrRYmWmbIt9@nlDJt@QNWZep%(xK2KX&69!(ZOJkS8RMklX7U",
				"H%5ds-%Bwy!j04AQYTAxi8CQdrc!l(0(sjr/+TqRlf(hn8!B8v7cETW0ZPaO+b9a",
				"HmIui0aWC?nnetU&esa30ywy462XS(O(d0&WX/p1g1jS%(LkK8iisAhegt@uDKUo",
				"d+TV+gG84!+8om!alMj5XVT5y?driIBd%vMLkBqJJnm57eEnpu74igAF$B9FQ?6r",
				"Ltvd1ND)2)QtI!TTOs$9-WnZwR!RLc&+UCk!9cyc%$VPKSm%OtvE/$IF$5U8?sq5",
				"Hs@F$RLF%VxZagQ9U7aes!)bw@O2c5Xnu11IR1EtX!NuC5TOt5kd7G/IhQXh4p8q",
				"8fqb$&%qcGmBn2fgh&PQI4bej2B(o)RnKO)l/T0TzvhGDR0IbHs)Lr?uRQHBrIO8",
				"H1-1gGD2+hAM1mH0PDF68WxdaZ&3OHt+)6QSl(pJdtNgZHMHOI!JhJZHw(lgRMpU"	
		};
		
	private static String bodyKey = "{4c9Kyyk+${)t2yJI+7W(%oI=GW)sVzM";
	private static String bodySalt = "yPyP/##jt=KB946Yz7tiq1eS7qwr/Hq6";
	
	public String crearJWT(Account user) {
		
		String nombreRol = null;
		 
	    // El algoritmo para firmar el token.
	    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
	 
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(new Date());
	    cal.add(Calendar.MINUTE, 20);
	    
	    int posicionIndice = getIndice(user.getName());
	 
	    // Firmar el token con el secreto.
	    byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(arrSecretos[posicionIndice]);
	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
	    
	    // Conseguir nombre del rol a partir de su ID.
	    try {
			Role rolEncontrado = RoleServices.buscarRol(user.getRoleFK());
			nombreRol = rolEncontrado.getName();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
	    // Configurar estructura del token.
	    JwtBuilder builder = Jwts.builder()
	                                .setIssuedAt(new Date())
	                                .setExpiration(cal.getTime())
	                                .setSubject(StringUtils.capitalize(user.getName()))
	                                .setIssuer("Santander Bank")
	                                .claim("Role", StringUtils.capitalize(nombreRol))
	                                .signWith(signingKey, signatureAlgorithm);
	    
	    // Retornar el token con el cuerpo encriptado.
	    String jwtEncriptado = encriptarCuerpo(builder.compact());	    
	    return jwtEncriptado;
	}
	
	//Sample method to validate and read the JWT
	public boolean validarToken(String jwt) {
		boolean verificado = false;
		String nombre = getSujeto(jwt);
		int posicionIndice = getIndice(nombre);
		
		try {
			// El token es confiable
			Jwts.parser()         
				       .setSigningKey(DatatypeConverter.parseBase64Binary(arrSecretos[posicionIndice]))
				       .parseClaimsJws(jwt).getBody();

		    verificado = true;

		} catch (JwtException e) {
			// Error, no confiar en este token.
		    verificado = false;
		}
		return verificado;
	}
	
	public static String getSujeto(String jwt) {
	        String[] split_string = jwt.split("\\.");
	        String base64EncodedBody = split_string[1];
	        Base64 base64Url = new Base64(true);

	        // Decodificar el cuerpo del JWT
	        String cuerpoJWT = new String(base64Url.decode(base64EncodedBody));
	        // Separar el cuerpo del JWT por comas.
	        String[] contenidoExtraido = cuerpoJWT.split(",");
	        // Separar valor de la llave 'Sujeto'
	        String[] sujetoExtraido = contenidoExtraido[2].split(":");
	        // Remover comillas del nombre
	        String sujetoJWT = sujetoExtraido[1].replaceAll("\"", "");
	        return sujetoJWT;
	}
	
	public static String getRol(String jwt) {
        String[] split_string = jwt.split("\\.");
        String base64EncodedBody = split_string[1];
        Base64 base64Url = new Base64(true);

        // Decodificar el cuerpo del JWT
        String cuerpoJWT = new String(base64Url.decode(base64EncodedBody));
        // Separar el cuerpo del JWT por comas.
        String[] contenidoExtraido = cuerpoJWT.split(",");

        // Extraer el rol del cuerpo.
        String[] rolExtraido = contenidoExtraido[4].split(":");
        // Remover las comillas y caracteres innecesarios.
        String rolJWT = rolExtraido[1].replaceAll("\"", "").replaceAll("}", "");
        return rolJWT;
	}
	
	public static int getIndice(String str) {
		int posicion = 0;
		if(!str.matches("[a-zA-Z ]+")) {
			return posicion;
		}
		char primerCaracter = str.toUpperCase().trim().charAt(0);
		String alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String[] arrAlfabeto = alfabeto.split("(?!^)");
		
		for(int i = 0; i < arrAlfabeto.length; i++) {
			if(arrAlfabeto[i].equalsIgnoreCase(Character.toString(primerCaracter))) {
				posicion = i;
				return posicion;
			}
		}
		return posicion;
	}
		
	public String getSecreto(int indice) {
		String secreto = null;
		if(indice < 0 || indice > 25) {
			return null;
		}
		else 
		{
			secreto = arrSecretos[indice];
			return secreto;
		}
	}
	
	public static String encriptarCuerpo(String rawJWT)
	{
	    try
	    {
	        byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	        IvParameterSpec ivspec = new IvParameterSpec(iv);
	         
	        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
	        KeySpec spec = new PBEKeySpec(bodyKey.toCharArray(), bodySalt.getBytes(), 65536, 256);
	        SecretKey tmp = factory.generateSecret(spec);
	        SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
	         
	        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	        cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
	        return java.util.Base64.getEncoder().encodeToString(cipher.doFinal(rawJWT.getBytes("UTF-8")));
	    }
	    catch (Exception e)
	    {
	        System.out.println("Error while encrypting: " + e.toString());
	    }
	    return null;
	}
	
	public static String desencriptarCuerpo(String jwtEncriptado) {
	    try
	    {
	        byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	        IvParameterSpec ivspec = new IvParameterSpec(iv);
	         
	        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
	        KeySpec spec = new PBEKeySpec(bodyKey.toCharArray(), bodySalt.getBytes(), 65536, 256);
	        SecretKey tmp = factory.generateSecret(spec);
	        SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
	         
	        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	        cipher.init(Cipher.DECRYPT_MODE, secretKey, ivspec);
	        return new String(cipher.doFinal(java.util.Base64.getDecoder().decode(jwtEncriptado)));
	    }
	    catch(IllegalArgumentException aException) {
	    	return null;
	    }
	    catch(IllegalBlockSizeException bException) {
	    	return null;
	    }
	    catch (Exception e) {
	        System.out.println("Error while decrypting: " + e.toString());
	    }
	    return null;
	}
			
}
