package com.jwt.learning.models;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Role {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ID;
	
	@NotNull(message = "El nombre del rol es obligatorio!")
	@Size(min = 3, max = 128, message = "El nombre del rol debe contener entre 3 y 128 caracteres!")
	@Column(name = "name", unique = true)
	private String name;
	
	// Constructores
	
	public Role() { }
	
	public Role(String name) {
		this.name = name.trim();
	}
	
	// Getters
	
	public int getID() {
		return this.ID;
	}
	
	public String getName() {
		return this.name;
	}
	
	// Setters
	
	public void setID(int id) {
		this.ID = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}

}
