package com.jwt.learning.db;

public interface IRole {
	
	void crearTablaRoles();
	
	void funcionEncontrarRol();
	
	void funcionEncontrarRolNombre();
	
	void funcionCrearRol();
	
	void funcionEditarRol();
	
	void funcionEliminarRol();
	
}
