package com.jwt.learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class JwtdemoApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(JwtdemoApplication.class, args);	
	}
	
	@Bean
	public Docket jwtAPI() {
	   return new Docket(DocumentationType.SWAGGER_2).select()
	   .apis(RequestHandlerSelectors.basePackage("com.jwt.learning")).build();
	}

}
