package com.jwt.learning.models;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

public class Account {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ID;
	
	@NotNull(message = "El nombre del usuario es obligatorio!")
	@Size(min = 3, max = 128, message = "El nombre del usuario debe contener entre 3 y 128 caracteres!")
	@Column(name = "name", unique = true)
	private String name;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	@Size(min = 12, max = 128, message = "La contraseña debe contener entre 12 y 128 caracteres!")
	private String password;
	
	@NotNull(message = "El ID de rol del usuario es obligatorio!")
	private int roleFK = 0;
	
	// Constructores
	
	public Account() { }
	
	public Account(String name, String password) {
		this.name = name.trim();
		this.password = password;
	}
	
	// Getters
	
	public int getID() {
		return this.ID;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public int getRoleFK() {
		return this.roleFK;
	}
	
	// Setters
	
	public void setID(int id) {
		this.ID = id;
	}
	
	public void setName(String name) {
		this.name = name.trim();
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setRoleFK(int id) {
		this.roleFK = id;
	}

}
