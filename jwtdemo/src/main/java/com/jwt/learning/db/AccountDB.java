package com.jwt.learning.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import com.jwt.learning.ConfigClass.DatabaseConfig;

public class AccountDB implements IAccount {

	public AccountDB(boolean ejecutar) {
		if(ejecutar) {
			crearTablaUsuario();
			funcionEncontrarUsuario();
			funcionEncontrarUsuarioNombre();
			funcionCrearUsuario();
			funcionEditarUsuario();
			funcionEliminarUsuario();
			funcionLogin();
			funcionConseguirContraseña();
			funcionCambiarContraseña();
		}
	}
	
	@Override
	public void crearTablaUsuario() {

		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	         String sql = "CREATE TABLE IF NOT EXISTS Account " +
	            "(ID INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, " +
	            "Name VARCHAR(128) NOT NULL UNIQUE, " +
	            "Password VARCHAR(128) NOT NULL, "
	            + "RoleFK INT NOT NULL,"
	            + "FOREIGN KEY(RoleFK) REFERENCES Role)";
	         
	         stmt.executeUpdate(sql);
	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	      }
	}

	@Override
	public void funcionEncontrarUsuario() {
		// TODO Auto-generated method stub

		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	         stmt.execute("DO $$\r\n" + 
		         		"BEGIN\r\n" + 
		        		 
		         		"IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'datosusuario') THEN\r\n" + 
		         		"CREATE TYPE datosusuario AS (idU INT, nombreU VARCHAR(128), rolU INT);\r\n" + 
		         		"END IF;\r\n" + 
		         		"END$$;" + 
		         		
		        		"CREATE OR REPLACE FUNCTION encontrarUsuario(usuarioID INT)\r\n" + 
		        		"RETURNS datosUsuario AS $$\r\n" +
		        		
		        		"DECLARE\r\n" + 
		        		"resultado datosUsuario;\r\n" +
		        		
			         	"BEGIN\r\n" + 
			         	
		         		"SELECT id, name, roleFK INTO resultado.idU, resultado.nombreU, resultado.rolU FROM account\r\n"
		         		+ "WHERE id = usuarioID;\r\n"
		         		+ "RETURN resultado;\r\n" +
		         		
		         		"END\r\n" +
		         		"$$ LANGUAGE plpgsql;");

	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	      }
	}
	
	@Override
	public void funcionEncontrarUsuarioNombre() {
		// TODO Auto-generated method stub

		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	         stmt.execute("DO $$\r\n" + 
		         		"BEGIN\r\n" + 
		        		 
		         		"IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'datosusuario') THEN\r\n" + 
		         		"CREATE TYPE datosusuario AS (idU INT, nombreU VARCHAR(128), rolU INT);\r\n" + 
		         		"END IF;\r\n" + 
		         		"END$$;" + 
		         		
		        		"CREATE OR REPLACE FUNCTION existeUsuario(nombreUsuario VARCHAR)\r\n" + 
		        		"RETURNS datosUsuario AS $$\r\n" +
		        		
		        		"DECLARE\r\n" + 
		        		"resultado datosUsuario;\r\n" +
		        		
			         	"BEGIN\r\n" + 
			         	
		         		"SELECT id, name, roleFK INTO resultado.idU, resultado.nombreU, resultado.rolU FROM account\r\n"
		         		+ "WHERE UPPER(name) = nombreUsuario;\r\n"
		         		+ "RETURN resultado;\r\n" +
		         		
		         		"END\r\n" +
		         		"$$ LANGUAGE plpgsql;");

	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	      }
	}

	@Override
	public void funcionCrearUsuario() {
		// TODO Auto-generated method stub

		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	         stmt.execute("DO $$\r\n" + 
	         		"BEGIN\r\n" + 
	        		 
	         		"IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'datosusuario') THEN\r\n" + 
	         		"CREATE TYPE datosusuario AS (idU INT, nombreU VARCHAR(128), rolU INT);\r\n" + 
	         		"END IF;\r\n" + 
	         		"END$$;" + 
	         		
	        		"CREATE OR REPLACE FUNCTION crearUsuario(nombreUsuario VARCHAR, contraseñaUsuario VARCHAR, rolID INT)\r\n" + 
	        		"RETURNS datosUsuario AS $$\r\n" +
	        		
	        		"DECLARE\r\n" + 
	        		"resultado datosUsuario;\r\n" +
	        		
		         	"BEGIN\r\n" + 
	         		"INSERT INTO account (name, password, roleFK) VALUES (nombreUsuario, contraseñaUsuario, rolID);\r\n" + 
		         	
	         		"SELECT id, name, roleFK INTO resultado.idU, resultado.nombreU, resultado.rolU FROM account ORDER BY id DESC LIMIT 1;\r\n"
	         		+ "RETURN resultado;\r\n" + 
	         		
	         		"END\r\n" +
	         		"$$ LANGUAGE plpgsql;");

	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	      }
	}

	@Override
	public void funcionEditarUsuario() {
		// TODO Auto-generated method stub

		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	         stmt.execute("DO $$\r\n" + 
		         		"BEGIN\r\n" + 
		        		 
		         		"IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'datosusuario') THEN\r\n" + 
		         		"CREATE TYPE datosusuario AS (idU INT, nombreU VARCHAR(128), rolU INT);\r\n" + 
		         		"END IF;\r\n" + 
		         		"END$$;" + 
		         		
		        		"CREATE OR REPLACE FUNCTION editarUsuario(usuarioID INT, usuarioNombre VARCHAR, nuevoRol INT)\r\n" + 
		        		"RETURNS datosUsuario AS $$\r\n" +
		        		
		        		"DECLARE\r\n" + 
		        		"resultado datosUsuario;\r\n" +
		        		
			         	"BEGIN\r\n" + 
		         		"UPDATE account SET name = usuarioNombre, roleFK = nuevoRol WHERE id = usuarioID;\r\n" + 
			         	
		         		"SELECT id, name, roleFK INTO resultado.idU, resultado.nombreU, resultado.rolU FROM account WHERE id = usuarioID;\r\n"
		         		+ "RETURN resultado;\r\n" + 
		         		
		         		"END\r\n" +
		         		"$$ LANGUAGE plpgsql;");

	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	      }
	}

	@Override
	public void funcionEliminarUsuario() {
		// TODO Auto-generated method stub

		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	         stmt.execute("DO $$\r\n" + 
		         		"BEGIN\r\n" + 
		        		 
		         		"IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'datosusuario') THEN\r\n" + 
		         		"CREATE TYPE datosusuario AS (idU INT, nombreU VARCHAR(128), rolU INT);\r\n" + 
		         		"END IF;\r\n" + 
		         		"END$$;" + 
		         		
		        		"CREATE OR REPLACE FUNCTION eliminarUsuario(usuarioID INT)\r\n" + 
		        		"RETURNS datosUsuario AS $$\r\n" +
		        		
		        		"DECLARE\r\n" + 
		        		"resultado datosUsuario;\r\n" +
		        		
			         	"BEGIN\r\n" + 
			         	
			         	"SELECT id, name, roleFK INTO resultado.idU, resultado.nombreU, resultado.rolU FROM account WHERE id = usuarioID;\r\n"
		         		+ "DELETE FROM account WHERE id = usuarioID;\r\n" 
			         	+ "RETURN resultado;\r\n" + 
			         	
		         		"END\r\n" +
		         		"$$ LANGUAGE plpgsql;");

	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	      }
	}

	@Override
	public void funcionLogin() {
		// TODO Auto-generated method stub

		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	         stmt.execute("DO $$\r\n" + 
	         		"BEGIN\r\n" + 
	        		 
	         		"IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'credencialesacceso') THEN\r\n" + 
	         		"CREATE TYPE credencialesacceso AS (nombreU VARCHAR(128), contraseñaU VARCHAR(128), rolID INT);\r\n" + 
	         		"END IF;\r\n" + 
	         		"END$$;" + 
	         		
	        		"CREATE OR REPLACE FUNCTION login(nombreUsuario VARCHAR)\r\n" + 
	        		"RETURNS credencialesacceso AS $$\r\n" +
	        		
	        		"DECLARE\r\n" + 
	        		"credenciales credencialesacceso;\r\n" +
	        		
		         	"BEGIN\r\n" + 
	         		"SELECT name, password, roleFK INTO credenciales.nombreU, credenciales.contraseñaU, credenciales.rolID "
	         		+ "FROM account WHERE UPPER(name) = nombreUsuario;\r\n"
	         		+ "RETURN credenciales;\r\n" + 
	         		
	         		"END\r\n" + 
	         		"$$ LANGUAGE plpgsql;");

	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	      }
	}
	
	@Override
	public void funcionConseguirContraseña() {
		// TODO Auto-generated method stub
		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	        stmt.execute("DO $$\r\n" + 
	         		"BEGIN\r\n" + 
	        		 
	         		"IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'cambiopass') THEN\r\n" + 
	         		"CREATE TYPE cambiopass AS (usuarioHash VARCHAR(128));\r\n" + 
	         		"END IF;\r\n" + 
	         		"END$$;" + 
	         		
	        		"CREATE OR REPLACE FUNCTION conseguirPass(nombreUsuario VARCHAR)\r\n" + 
	        		"RETURNS cambiopass AS $$\r\n" +
	        		
	        		"DECLARE\r\n" + 
	        		"resultado cambiopass;\r\n" +
	        		
		         	"BEGIN\r\n" + 
		         	
	         		"SELECT password INTO resultado.usuarioHash FROM account\r\n"
	         		+ "WHERE UPPER(name) = nombreUsuario;\r\n"
	         		+ "RETURN resultado;\r\n" +
	         		
	         		"END\r\n" +
	         		"$$ LANGUAGE plpgsql;");

	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	      }
	}

	@Override
	public void funcionCambiarContraseña() {
		// TODO Auto-generated method stub

		Connection c = null;
	    Statement stmt = null;
	    
		try {
			c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
	        stmt = c.createStatement();
	         
	         stmt.execute(
	        		"CREATE OR REPLACE FUNCTION cambiarPass(nombreUsuario VARCHAR, nuevaContraseña VARCHAR)\r\n" + 
	        		"RETURNS void AS $$\r\n" +
	     
		         	"BEGIN\r\n" + 
	         		"UPDATE account SET password = nuevaContraseña WHERE UPPER(name) = nombreUsuario;\r\n" +
	         		"END\r\n" + 
	         		
	         		"$$ LANGUAGE plpgsql;");

	         stmt.close();
	         c.close();
	         
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	      }
	}

}
