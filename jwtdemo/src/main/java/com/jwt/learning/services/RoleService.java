package com.jwt.learning.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Service;
import com.jwt.learning.ConfigClass.DatabaseConfig;
import com.jwt.learning.models.Role;

@Service
public class RoleService {

	public static class RoleServices {
		
		// Retorna un rol buscandolo por el ID.
		public static Role buscarRol(int id) throws SQLException {

			// Inicializacion de variables.
			String datosRol = null;
			Connection c = null;
			Role rolEncontrado = new Role();
			
			try {
				 // Conexion a la base de datos.
				 c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
		         // Desactivar el auto commit para realizar transacciones y asegurar la integridad de la BD.
		         c.setAutoCommit(false);

		         // Ejecutar la funcion en PostgreSQL con sus respectivos parametros (ID)
		         PreparedStatement pstmt = c.prepareStatement("SELECT encontrarRol(?)");
		         pstmt.setInt(1, id);
		         pstmt.execute();
		         
		         // Obtener los valores retornados por la funcion ejecutada
		         ResultSet results = (ResultSet) pstmt.getResultSet();
		         while (results.next()) {
		             datosRol = results.getString(1);
		         }
		         // La funcion no encontro ningun rol con ese ID, retornar nulo.
		         if(datosRol == null) {
		        	 return null;
		         }
		         // Cerrar la conexion y el PreparedStatement.   
		         c.commit();
		         pstmt.close();
		         
		         // Obtener los valores necessarios por medio de expresiones regulares.
		         int rolID = Integer.parseInt(datosRol.replaceAll("\\D+",""));
		         String nombreRol = datosRol.replaceAll("[^A-Za-z ]+","");
		         
		         // Rellenar el objeto con los datos retornados de la funcion.
		         rolEncontrado.setID(rolID);
		         rolEncontrado.setName(nombreRol);
		         
		      } catch ( Exception e ) {
		    	  // En caso de error, retornar los cambios hechos en la BD con un rollback.
		         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		         c.rollback();
		      }
			finally 
			{
				// Por medida preventiva, agregar un fin de conexion que siempre se ejecute.
				if(c != null) c.close();
			}
	        return rolEncontrado;
		}
		
		// Retorna un rol por medio de su nombre.
		public static Role buscarRol(String nombre) throws SQLException {

			// Inicializacion de variables.
			String datosRol = null;
			Connection c = null;
			Role rolEncontrado = new Role();
			
			try {
				 // Conexion a la base de datos.
				c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
		         // Desactivar el auto commit para realizar transacciones y asegurar la integridad de la BD.
		         c.setAutoCommit(false);

		         // Ejecutar la funcion en PostgreSQL con sus respectivos parametros (Nombre del rol a encontrar)
		         PreparedStatement pstmt = c.prepareStatement("SELECT existeRol(?)");
		         pstmt.setString(1, nombre.toUpperCase().trim());
		         pstmt.execute();
		         
		         // Obtener los valores retornados por la funcion ejecutada
		         ResultSet results = (ResultSet) pstmt.getResultSet();
		         while (results.next()) {
		             datosRol = results.getString(1);
		         }
		         // La funcion no encontro ningun rol con ese nombre, retornar nulo.
		         if(datosRol == null) {
		        	 return null;
		         }
		         // Cerrar la conexion y el PreparedStatement.   
		         c.commit();
		         pstmt.close();
		         
		         // Obtener los valores necessarios por medio de expresiones regulares.
		         int rolID = Integer.parseInt(datosRol.replaceAll("\\D+",""));
		         String nombreRol = datosRol.replaceAll("[^A-Za-z ]+","");
		         
		         // Rellenar el objeto con los datos retornados de la funcion.
		         rolEncontrado.setID(rolID);
		         rolEncontrado.setName(nombreRol);
		         
		      } catch ( Exception e ) {
		    	  // En caso de error, retornar los cambios hechos en la BD con un rollback.
		         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		         c.rollback();
		      }
			finally 
			{
				// Por medida preventiva, agregar un fin de conexion que siempre se ejecute.
				if(c != null) c.close();
			}
	        return rolEncontrado;
		}
		
		// Crea un nuevo rol.
		public static Role crearRol(Role rol) throws SQLException {
			
			// Inicializacion de variables.
			Connection c = null;
			Role rolCreado = new Role();
			String datosRol = null;
			
			try {
				 // Conexion a la base de datos.
				c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
		         // Desactivar el auto commit para realizar transacciones y asegurar la integridad de la BD.
		         c.setAutoCommit(false);
		         
		         // Ejecutar la funcion en PostgreSQL con sus respectivos parametros (Objeto de Clase Rol)
		         PreparedStatement pstmt = c.prepareStatement("SELECT crearRol(?)");
		         pstmt.setString(1, rol.getName().trim());
		         pstmt.execute();
		         
		         // Obtener los valores retornados por la funcion ejecutada
		         ResultSet results = (ResultSet) pstmt.getResultSet();
		         while (results.next()) {
		             datosRol = results.getString(1);
		         }
		            
		         // Cerrar la conexion y el PreparedStatement.   
		         c.commit();
		         pstmt.close();
		         
		         // Obtener los valores necessarios por medio de expresiones regulares.
		         int rolID = Integer.parseInt(datosRol.replaceAll("\\D+",""));
		         String nombreRol = datosRol.replaceAll("[^A-Za-z ]+","");
		         
		         // Rellenar el objeto con los datos retornados de la funcion.
		         rolCreado.setID(rolID);
		         rolCreado.setName(nombreRol);
		         
		      } catch ( Exception e ) {
		    	 // En caso de error, retornar los cambios hechos en la BD con un rollback.
		         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		         c.rollback();
		      }
			finally 
			{
				// Por medida preventiva, agregar un fin de conexion que siempre se ejecute.
				if(c != null) c.close();
			}
	        return rolCreado;
		}
		
		// Modifica un rol existente.
		public static Role editarRol(int id, Role rol) throws SQLException {
		
			// Inicializacion de variables.
			Connection c = null;
			Role rolEditado = new Role();
			String datosRol = null;
			
			try {
				// No existe un rol con esta ID.
				if(buscarRol(id) == null) {
					return null;
				}
				 // Conexion a la base de datos.
				c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
		         // Desactivar el auto commit para realizar transacciones y asegurar la integridad de la BD.
		         c.setAutoCommit(false);

		         // Ejecutar la funcion en PostgreSQL con sus respectivos parametros (int ID, nuevo nombre del rol)
		         PreparedStatement pstmt = c.prepareStatement("SELECT editarRol(?, ?)");
		         pstmt.setInt(1, id);
		         pstmt.setString(2, rol.getName());
		         pstmt.execute();
		         
		         // Obtener los valores retornados por la funcion ejecutada
		         ResultSet results = (ResultSet) pstmt.getResultSet();
		         while (results.next()) {
		             datosRol = results.getString(1);
		         }
		         // No existe rol con este ID, retornar nulo.
		         if(datosRol == null) {
		        	 return null;
		         }
		         // Cerrar la conexion y el PreparedStatement.   
		         c.commit();
		         pstmt.close();
		         
		         // Obtener los valores necessarios por medio de expresiones regulares.
		         int rolID = Integer.parseInt(datosRol.replaceAll("\\D+",""));
		         String nombreRol = datosRol.replaceAll("[^A-Za-z ]+","");
		         
		         // Rellenar el objeto con los datos retornados de la funcion.
		         rolEditado.setID(rolID);
		         rolEditado.setName(nombreRol);
		         
		      } catch ( Exception e ) {
		    	 // En caso de error, retornar los cambios hechos en la BD con un rollback.
		         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		         c.rollback();
		      }
			finally 
			{
				// Por medida preventiva, agregar un fin de conexion que siempre se ejecute.
		        if(c != null) c.close();
			}
	        return rolEditado;
		}
		
		// Elimina un rol existente.
		public static Role eliminarRol(int id) throws SQLException {
			
			// Inicializacion de variables.
			Connection c = null;
			Role rolEliminado = new Role();
			String datosRol = null;
			
			try {
				// No existe un rol con esta ID.
				if(buscarRol(id) == null) {
					return null;
				}
				 // Conexion a la base de datos.
				c = DriverManager.getConnection(DatabaseConfig.dbURL, DatabaseConfig.dbUsername, DatabaseConfig.dbPassword);
		         // Desactivar el auto commit para realizar transacciones y asegurar la integridad de la BD.
		         c.setAutoCommit(false);

		         // Ejecutar la funcion en PostgreSQL con sus respectivos parametros (ID del rol a eliminar)
		         PreparedStatement pstmt = c.prepareStatement("SELECT eliminarRol(?)");
		         pstmt.setInt(1, id);
		         pstmt.execute();
		         
		         // Obtener los valores retornados por la funcion ejecutada
		         ResultSet results = (ResultSet) pstmt.getResultSet();
		         while (results.next()) {
		             datosRol = results.getString(1);
		         }
		         // No existe rol con este ID, retornar nulo.
		         if(datosRol == null) {
		        	 return null;
		         }   
		         // Cerrar la conexion y el PreparedStatement.   
		         c.commit();
		         pstmt.close();
		         
		         // Obtener los valores necessarios por medio de expresiones regulares.
		         int rolID = Integer.parseInt(datosRol.replaceAll("\\D+",""));
		         String nombreRol = datosRol.replaceAll("[^A-Za-z ]+","");
		         
		         // Rellenar el objeto con los datos retornados de la funcion.
		         rolEliminado.setID(rolID);
		         rolEliminado.setName(nombreRol);
		         
		      } catch ( Exception e ) {
		    	 // En caso de error, retornar los cambios hechos en la BD con un rollback.
		         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		         c.rollback();
		      }
			finally 
			{
				// Por medida preventiva, agregar un fin de conexion que siempre se ejecute.
				if(c != null) c.close();
			}
	        return rolEliminado;
		}
	
	}

}
