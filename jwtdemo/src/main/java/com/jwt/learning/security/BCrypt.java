package com.jwt.learning.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BCrypt {
	
	public static class Encriptacion {
		
		public static String encriptarContraseña(String contraseña) {
			BCryptPasswordEncoder codificador = new BCryptPasswordEncoder(15);
			String contraseñaEncriptada = codificador.encode(contraseña);
			return contraseñaEncriptada;
		}
		
		public static boolean verificarContraseña(String contraseña, String contraseñaEncriptada) {
			boolean resultado = false;
			BCryptPasswordEncoder codificador = new BCryptPasswordEncoder(15);
			resultado = codificador.matches(contraseña, contraseñaEncriptada);
			return resultado;
		}
		
	}

}
